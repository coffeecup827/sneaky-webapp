/*global Materialize*/
import {web} from '../site';
import http from 'http';
import {status} from '../helpers/status';
import {updateUser} from '../actions/userActions';

export const request = {
  "post" : post,
  "put" : put
}

async function post(params,callback){
  if (params.token_exp && (new Date().getTime()) >= params.token_exp) {
    await params.googleUser.reloadAuthResponse().then((newGoogleUser) => {
      params.dispatch(updateUser({
        id_token : newGoogleUser.id_token,
        token_exp : newGoogleUser.expires_at
      }));
      params.body.id_token = newGoogleUser.id_token;
    })
  }
  var options = {
    host: web.host,
    port: process.env.NODE_ENV !== "development" ? null : 300,
    path: params.path,
    method: 'POST',
    headers: {'Content-Type': 'application/json'},
    withCredentials : true
  };
  var req = http.request(options,responseParser.bind({
    callback,
    requestFailedAction : params.requestFailedAction
  }));

  req.write(JSON.stringify(params.body));

  req.end();
}

async function put(params,callback){
  if (params.token_exp && (new Date().getTime()) >= params.token_exp) {
    await params.googleUser.reloadAuthResponse().then((newGoogleUser) => {
      params.dispatch(updateUser({
        id_token : newGoogleUser.id_token,
        token_exp : newGoogleUser.expires_at
      }));
      params.body.id_token = newGoogleUser.id_token;
    })
  }
  var options = {
    host: web.host,
    port: process.env.NODE_ENV !== "development" ? null : 300,
    path: params.path,
    method: 'PUT',
    headers: {'Content-Type': 'application/json'},
    withCredentials : true
  };
  var req = http.request(options,responseParser.bind({
    callback
  }));

  req.write(JSON.stringify(params.body));

  req.end();
}

function responseParser(response){
  response.on('data', (res) => {
    const data = JSON.parse(res);
    if (data.status === status.SessionExpired) {
      window.location.reload(true);
    }
    if (data.status === status.Failed) {
      if (this.requestFailedAction) {
        this.requestFailedAction();
      }
      Materialize.Toast.removeAll();
      Materialize.toast(data.message, 3000,'grey darken-4 red-text font-weight-400');
    }
    else{
      this.callback(data);
    }
  }).on('error',(err) => {
    if (err.code === 'ETIMEDOUT') {
      if (this.requestFailedAction) {
        this.requestFailedAction();
      }
    }
  });
}
