/*global Materialize*/
import openSocket from 'socket.io-client';
import {status} from '../helpers/status';
import {updateDeviceStatus,updateDevices} from '../actions/userActions'
import {deviceActions,addMessages,updateMessagePreference,updatePreferenceById} from '../actions/deviceActions'
import {MessageTypes} from '../helpers/messgeType'

let socket = null;

export const Socket = {
  connect : connect,
  disconnect : disconnect,
  joinRoom : joinRoom
}

async function connect(params){
  console.log(process.env);
  socket = socket ? socket : openSocket(process.env.NODE_ENV !== "development" ? 'https://sneaky-dev-server.herokuapp.com' : 'http://localhost:300',{transports: ['websocket']});
  /* Subsribe to events on connection */

  socket.on('connect', function() {
    params.signInActionParam.socketId = socket.id
    params.signInAction(params.signInActionParam)
  });

  socket.on('room-joined',function(response){
    socket.deviceId = response.data.deviceId;
    params.dispatch(updateDeviceStatus({
      deviceId : socket.deviceId,
      IsLoggedIn : true
    }))
  })

  socket.on('device-status', function(response) {
    actionEventResponseHandler(response,(response) => {
      params.dispatch(updateDeviceStatus(response.data))
    });
  });

  socket.on('message-sent',function (response){
    
    actionEventResponseHandler(response,(response) => {
      if (response.data.socketId === socket.id) return;

      let message = response.data.message

      let messagePreference = {}

      message.messagePreference.map(p => (
        messagePreference[p.DeviceId] = p
      ))

      message.messagePreference = messagePreference

      params.dispatch(addMessages({
        messages : {
          [message._id] : message
        },
        messageType : MessageTypes.NewMessage,
        fetchType : deviceActions.ADD_TO_SNEAKBOX
      }))
      params.dispatch(addMessages({
        messages : {
          [message._id] : message
        },
        messageType : MessageTypes.NewMessage,
        fetchType : deviceActions.ADD_TO_SENT
      }))
    })
  })

  socket.on('message-received', function(response) {
    actionEventResponseHandler(response,(response) => {

      let message = response.data.message

      let messagePreference = {}

      message.messagePreference.map(p => (
        messagePreference[p.DeviceId] = p
      ))

      message.messagePreference = messagePreference

      params.dispatch(addMessages({
        messages : {
          [message._id] : message
        },
        messageType : MessageTypes.NewMessage,
        fetchType : deviceActions.ADD_TO_SNEAKBOX
      }))
      params.dispatch(addMessages({
        messages : {
          [message._id] : message
        },
        messageType : MessageTypes.NewMessage,
        fetchType : deviceActions.ADD_TO_INBOX
      }))
    });
  });

  socket.on('update-message-preference',function(response){
    actionEventResponseHandler(response,(response) => {
      params.dispatch(updateMessagePreference(response.data))
    })
  })

  socket.on('update-device-preference',function(response){
    actionEventResponseHandler(response,(response) => {
      params.dispatch(updatePreferenceById(response.data))
    })
  })

  socket.on('new-device-added',function(response){
    actionEventResponseHandler(response,(response) => {
      params.dispatch(updateDevices(response.data))
    })
  })

  socket.on('disconnect',function(){
    params.dispatch(updateDeviceStatus({
      deviceId : socket.deviceId,
      IsLoggedIn : false
    }))
  })

}

async function disconnect(params){
  socket.emit('logout-user')
  socket.destroy();
  socket = null;
  console.log("Disconnected");
}

async function joinRoom(params){
  socket.emit('join-room', params, infoEventResponseHandler);
}

function infoEventResponseHandler(response){
  if (response.status === status.Success){
    Materialize.toast(response.message, 2000,'grey darken-4 green-text font-weight-400');
  }
  else{
    Materialize.toast(response.message, 3000,'grey darken-4 red-text font-weight-400');
  }
}

function actionEventResponseHandler(response,callback){
  if (response.status === status.Success){
    Materialize.toast(response.message, 2000,'grey darken-4 green-text font-weight-400');

    callback(response);
  }
  else{
    Materialize.toast(response.message, 3000,'grey darken-4 red-text font-weight-400');
  }
}