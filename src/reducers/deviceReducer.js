import { deviceActions } from '../actions/deviceActions';
import {MessageTypes} from '../helpers/messgeType'

const initialState = {
  currentDevice : {
    DeviceId : null,
    DeviceName : null,
    Preferences : {}
  },
  SneakBox : null,
  Inbox :  null,
  Sent :  null,
  SneakBoxPaginator : 0,
  InboxPaginator : 0,
  SentPaginator : 0,
  SneakBoxFetchType : "",
  InboxFetchType : "",
  SentFetchType : ""
}

export function deviceReducer(state = initialState, action){

  switch (action.type) {

    case deviceActions.UPDATE_CURRENT_DEVICE:
      return Object.assign({}, state, {
        currentDevice : {
          ...state.currentDevice,
          ...action.data
        }
      })

    case deviceActions.DELETE_DEVICE_DATA:
      return Object.assign({}, state, {
        ...initialState
      })

    case deviceActions.UPDATE_PREFERENCES:
      return {
        ...state,
        currentDevice : {
          ...state.currentDevice,
          ...action.data
        }
      }
    case deviceActions.UPDATE_PREFERENCE_BY_ID:
      let preference = action.data.preference;
      
      return {
        ...state,
        currentDevice : {
          ...state.currentDevice,
          Preferences : {
            ...state.currentDevice.Preferences,
            [preference.preferenceId] : {
              ...state.currentDevice.Preferences[preference.preferenceId],
              IsIncluded : preference.isIncluded
            }
          }
        }
      }

    case deviceActions.UPDATE_MESSAGE_PREFRENCE:
      return {
        ...state,
        SneakBox : !state.SneakBox ? state.SneakBox : !state.SneakBox[action.data.messageId] ? state.SneakBox : {
          ...state.SneakBox,
          [action.data.messageId] : {
            ...state.SneakBox[action.data.messageId],
            messagePreference : {
              ...state.SneakBox[action.data.messageId].messagePreference,
              [action.data.deviceId] : {
                ...state.SneakBox[action.data.messageId].messagePreference[action.data.deviceId],
                IsReceived : action.data.IsReceived,
                ReceivedAt : action.data.ReceivedAt
              }
            },
            isReadByAll : action.data.isReadByAll
          }
        },
        Inbox : !state.Inbox ? state.Inbox : !state.Inbox[action.data.messageId] ? state.Inbox : {
          ...state.Inbox,
          [action.data.messageId] : {
            ...state.Inbox[action.data.messageId],
            messagePreference : {
              ...state.Inbox[action.data.messageId].messagePreference,
              [action.data.deviceId] : {
                ...state.Inbox[action.data.messageId].messagePreference[action.data.deviceId],
                IsReceived : action.data.IsReceived,
                ReceivedAt : action.data.ReceivedAt
              }
            },
            isReadByAll : action.data.isReadByAll
          }
        } 
      }

    case deviceActions.ADD_TO_SNEAKBOX:

      if(MessageTypes.NewMessage === action.data.messageType){
        return {
          ...state,
          SneakBox : {
            ...state.SneakBox,
            ...action.data.messages
          },
          SneakBoxPaginator : state.SneakBoxPaginator + Object.keys(action.data.messages).length,
          SneakBoxFetchType : action.data.messageType
        }
      }

      return {
        ...state,
        SneakBox : {
          ...action.data.messages,
          ...state.SneakBox
        },
        SneakBoxPaginator : state.SneakBoxPaginator + Object.keys(action.data.messages).length,
        SneakBoxFetchType : action.data.messageType
      }

      case deviceActions.ADD_TO_INBOX:

      if(MessageTypes.NewMessage === action.data.messageType){
        return {
          ...state,
          Inbox : {
            ...state.Inbox,
            ...action.data.messages
          },
          InboxPaginator : state.InboxPaginator + Object.keys(action.data.messages).length,
          InboxFetchType : action.data.messageType
        }
      }

      return {
        ...state,
        Inbox : {
          ...action.data.messages,
          ...state.Inbox
        },
        InboxPaginator : state.InboxPaginator + Object.keys(action.data.messages).length,
        InboxFetchType : action.data.messageType
      }

      case deviceActions.ADD_TO_SENT:

      if(MessageTypes.NewMessage === action.data.messageType){
        return {
          ...state,
          Sent : {
            ...state.Sent,
            ...action.data.messages
          },
          SentPaginator : state.SentPaginator + Object.keys(action.data.messages).length,
          SentFetchType : action.data.messageType
        }
      }

      return {
        ...state,
        Sent : {
          ...action.data.messages,
          ...state.Sent
        },
        SentPaginator : state.SentPaginator + Object.keys(action.data.messages).length,
        SentFetchType : action.data.messageType
      }
    default:
      return state;
  }
}
