import { combineReducers } from 'redux';
import { userReducer } from './userReducers';
import {deviceReducer} from './deviceReducer'

const reducer = combineReducers({
  userReducer,
  deviceReducer
});

export default reducer
