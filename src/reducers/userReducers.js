import { userActions } from '../actions/userActions';

const initialState = {
  id_token : "",
  user : {},
  isLoggedIn : false,
  message : '',
  redirectTo : '',
  devices : {},
  deviceCount : 0,
  token_exp : 0,
  googleUser : null,
  socketId : "",
  session : ""
}

export function userReducer(state = initialState, action){

  switch (action.type) {

    case userActions.SIGN_IN:
      return Object.assign({}, state, {
        ...action.data
      })

    case userActions.SIGN_OUT:
      return Object.assign({}, state, {
        id_token : "",
        user : {},
        isLoggedIn : false,
        message : '',
        redirectTo : '',
        devices : {},
        deviceCount : 0,
        token_exp : 0,
        googleUser : null,
        socketId : "",
        session : ""
      })

    case userActions.UPDATE_USER:
      return Object.assign({}, state, {
        ...action.data
      })

    case userActions.UPDATE_DEVICES:
      return Object.assign({}, state, {
        ...state,
        devices : {
          ...state.devices,
          ...action.data
        }
      })
    case userActions.UPDATE_DEVICE_STATUS:
      return Object.assign({}, state, {
        ...state,
        devices : {
          ...state.devices,
          [action.data.deviceId] : {
            ...state.devices[action.data.deviceId],
            IsLoggedIn : action.data.IsLoggedIn
          }
        }
      })

    default:
      return state;
  }

}
