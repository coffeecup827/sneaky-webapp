export const web = {
  host : process.env.NODE_ENV !== "development" ? "sneaky-dev-server.herokuapp.com" : "localhost"
}
