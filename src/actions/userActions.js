/*global gapi,Materialize*/

import {status} from '../helpers/status'
import {request} from '../helpers/request'
import {updateCurrentDevice,deleteDeviceData} from './deviceActions';
import {Socket} from '../helpers/socket';

export const userActions = {
  "SIGN_IN" : "SIGN_IN",
  "SIGN_OUT" : "SIGN_OUT",
  "UPDATE_USER" : "UPDATE_USER",
  "UPDATE_DEVICES" : "UPDATE_DEVICES",
  "UPDATE_DEVICE_STATUS" : "UPDATE_DEVICE_STATUS"
}

const signin = (data) => ({
  type : userActions.SIGN_IN,
  data
})

const signOut = () => ({
  type : userActions.SIGN_OUT
})

export const updateDevices = (data) => ({
  type : userActions.UPDATE_DEVICES,
  data
})

export const updateUser = (data) => ({
  type : userActions.UPDATE_USER,
  data
})

export const updateDeviceStatus = (data) => ({
  type : userActions.UPDATE_DEVICE_STATUS,
  data
})

export const fetchDevices = (data) => {
  return function(dispatch){
    request.post({
      path : '/devices/userDevices',
      body : {
        id_token : data.id_token,
        session : data.session
      },
      dispatch,
      googleUser : data.googleUser,
      token_exp : data.token_exp
    },processUpdateDevicesData.bind({dispatch}));
  }
}

export const signOutUser = (data) => {
  return function (dispatch) {
    request.post({
      path : '/user/logout',
      body : {
        id_token : data.id_token,
        session : data.session
      },
      dispatch,
      googleUser : data.googleUser,
      token_exp : data.token_exp
    },processSignOut.bind({dispatch,deviceId : data.deviceId}));
  }
}

export const signinUser = (data) => {

  return async function (dispatch) {

    const googleUserData = await data.googleUser.reloadAuthResponse();

    const id_token = googleUserData.id_token;
    const token_exp = googleUserData.expires_at;
    console.log(id_token);

    request.post({
      path : '/user/signin',
      body : {
        id_token,
        socketId : data.socketId
      },
      dispatch
    },processSignInData.bind({dispatch,id_token,token_exp,googleUser : data.googleUser,socketId : data.socketId}));

  }
}

async function processSignInData(response) {
    console.log(response.data.email + " " + response.status);
    console.log(response.data);
    if (response.status === status.UserCreated || response.status === status.UserExist) {
      Materialize.toast(response.message, 1000,'grey darken-4');

      let preferences = {}
      response.data.Preferences.map(preference => (
        preferences[preference.PreferenceId] = preference
      ));
      this.dispatch(updateCurrentDevice({
        DeviceId : response.data.CurrentDevice,
        DeviceName : response.data.DeviceName,
        Preferences : preferences
      }))

      if (response.data.CurrentDevice) {
        Socket.joinRoom({
          session : response.data.session,
          deviceId : response.data.CurrentDevice,
          id_token : this.id_token
        });
      }
      
      let devices = {};
      response.data.Devices.map((device) => {
        devices[device._id] = device
      })

      this.dispatch(signin({
        id_token : this.id_token,
        user : response.data,
        message : response.message,
        isLoggedIn : true,
        redirectTo : response.data.CurrentDevice === null ? '/devices' : '/dashboard',
        devices : devices,
        deviceCount : response.data.Devices.length,
        token_exp : this.token_exp,
        googleUser : this.googleUser,
        socketId : this.socketId,
        session : response.data.session
      }));
    }
}

export function processSignOut(response){
  Socket.disconnect({
    deviceId : this.deviceId
  });
  this.dispatch(signOut());
  this.dispatch(deleteDeviceData());
  var auth2 = gapi.auth2.getAuthInstance();
  auth2.signOut().then(function () {
    Materialize.toast(response.message, 1000,'grey darken-4');
  });
}

function processUpdateDevicesData(response){

  let devices = {};
  response.data.Devices.map((device) => {
    devices[device._id] = device
  })

  this.dispatch(updateDevices(devices));
}
