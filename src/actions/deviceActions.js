/*global Materialize*/

import {status} from '../helpers/status'
import {request} from '../helpers/request'
import {Socket} from '../helpers/socket';
import {updateDevices} from './userActions'

export const deviceActions = {
  "UPDATE_CURRENT_DEVICE" : "UPDATE_CURRENT_DEVICE",
  "UPDATE_PREFERENCES" : "UPDATE_PREFERENCES",
  "UPDATE_PREFERENCE_BY_ID" : "UPDATE_PREFERENCE_BY_ID",
  "ADD_MESSAGES" : "ADD_MESSAGES",
  "DELETE_DEVICE_DATA" : "DELETE_DEVICE_DATA",
  "ADD_TO_SNEAKBOX" : "ADD_TO_SNEAKBOX",
  "ADD_TO_INBOX" : "ADD_TO_INBOX",
  "ADD_TO_SENT" : "ADD_TO_SENT",
  "UPDATE_MESSAGE_PREFRENCE" : "UPDATE_MESSAGE_PREFRENCE"
}

export const updateCurrentDevice = (data) => ({
  type : deviceActions.UPDATE_CURRENT_DEVICE,
  data
})

const updateDevicePreferences = (data) => ({
  type : deviceActions.UPDATE_PREFERENCES,
  data
})

export const updatePreferenceById = (data) => ({
  type : deviceActions.UPDATE_PREFERENCE_BY_ID,
  data
})

export const addMessages = (data) => ({
  type : data.fetchType,
  data : data
})

export const deleteDeviceData = () => ({
  type : deviceActions.DELETE_DEVICE_DATA
})

export const updateMessagePreference = (data) => ({
  type : deviceActions.UPDATE_MESSAGE_PREFRENCE,
  data
})

export const createDevice = (data) => {
  return function(dispatch){
    request.post({
      path : '/devices/create',
      body : {
        id_token : data.id_token,
        "device" : {
          "DeviceName" : data.deviceName,
          "Type" : "web"
        },
        session : data.session
      },
      dispatch,
      googleUser : data.googleUser,
      token_exp : data.token_exp
    },createDeviceResponseParser.bind({dispatch,session : data.session,id_token : data.id_token,}));
  }
}

export const selectDevice = (data) => {
  return function(dispatch){
    request.post({
      path : '/devices/select',
      body : {
        id_token : data.id_token,
        device : data.device,
        session : data.session
      },
      dispatch,
      googleUser : data.googleUser,
      token_exp : data.token_exp
    },processDeviceSelect.bind({dispatch,session : data.session,id_token : data.id_token,}));
  }
}

export const fetchDevicePreference = (data) => {
  return function(dispatch){
    request.post({
      path : '/devices/preferences',
      body : {
        id_token : data.id_token,
        deviceId : data.deviceId,
        session : data.session
      },
      dispatch,
      googleUser : data.googleUser,
      token_exp : data.token_exp
    },processUpdatePreference.bind({dispatch}));
  }
}

export const updateDevicePreferenceById = (data) => {
  return function(dispatch){
    request.put({
      path : '/devices/preferences',
      body : {
        id_token : data.id_token,
        deviceId : data.deviceId,
        preferenceId : data.preferenceId,
        isIncluded : data.isIncluded,
        session : data.session
      },
      dispatch,
      googleUser : data.googleUser,
      token_exp : data.token_exp
    },processUpdateDevicePreferenceById.bind({dispatch}));
  }
}

export const sendMessage = (data) => {
  return function(dispatch){
    request.put({
      path : '/messages/send',
      body : {
        id_token : data.id_token,
        deviceId : data.deviceId,
        message : data.message,
        session : data.session
      },
      dispatch,
      googleUser : data.googleUser,
      token_exp : data.token_exp,
      requestFailedAction : () => data.requestFailedAction()
    },processSendMessage.bind({
      dispatch,
      messageType : data.messageType,
      postSentAction : (messageId) => {data.postSentAction(messageId)}
    }));
  }
}

export const messageReceived = (data) => {
  return function(dispatch){
    request.put({
      path : '/messages/read',
      body : {
        id_token : data.id_token,
        deviceId : data.deviceId,
        messageId : data.messageId,
        session : data.session
      },
      dispatch,
      googleUser : data.googleUser,
      token_exp : data.token_exp
    },processMessageReceived.bind({
      dispatch
    }));
  }
}

export const fetchForSneakBox = (data) => {
  return function(dispatch){
    request.post({
      path : '/messages/sneakbox',
      body : {
        id_token : data.id_token,
        deviceId : data.deviceId,
        session : data.session,
        skipTo : data.skipTo
      },
      dispatch,
      googleUser : data.googleUser,
      token_exp : data.token_exp
    },processfetchMessages.bind({
      dispatch,
      messageType : data.messageType,
      fetchType : deviceActions.ADD_TO_SNEAKBOX
    }));
  }
}

export const fetchForInbox = (data) => {
  return function(dispatch){
    request.post({
      path : '/messages/inbox',
      body : {
        id_token : data.id_token,
        deviceId : data.deviceId,
        session : data.session,
        skipTo : data.skipTo
      },
      dispatch,
      googleUser : data.googleUser,
      token_exp : data.token_exp
    },processfetchMessages.bind({
      dispatch,
      messageType : data.messageType,
      fetchType : deviceActions.ADD_TO_INBOX
    }));
  }
}

export const fetchForSent = (data) => {
  return function(dispatch){
    request.post({
      path : '/messages/sent',
      body : {
        id_token : data.id_token,
        deviceId : data.deviceId,
        session : data.session,
        skipTo : data.skipTo
      },
      dispatch,
      googleUser : data.googleUser,
      token_exp : data.token_exp
    },processfetchMessages.bind({
      dispatch,
      messageType : data.messageType,
      fetchType : deviceActions.ADD_TO_SENT
    }));
  }
}

async function createDeviceResponseParser(response) {
  if (response.status === status.Success){

    Materialize.toast(response.message, 2000,'grey darken-4');

    Socket.joinRoom({
      session : this.session,
      deviceId : response.data.DeviceId,
      id_token : this.id_token
    });

    this.dispatch(updateDevices({
     [response.data.DeviceId] : {
        _id : response.data.DeviceId,
        DeviceName : response.data.DeviceName,
        Type : response.data.Type,
        IsLoggedIn : response.data.IsLoggedIn
      }
    }))

    let preferences = {}
    response.data.Preferences.map(preference => (
      preferences[preference.PreferenceId] = preference
    ));

    this.dispatch(updateCurrentDevice({
      DeviceId : response.data.DeviceId,
      DeviceName : response.data.DeviceName,
      Preferences : preferences
    }));
  }
}

async function processDeviceSelect(response){
  if (response.status === status.Success){
    Materialize.toast(response.message, 2000,'grey darken-4 green-text font-weight-400');

    let preferences = {}
    response.data.Preferences.map(preference => (
      preferences[preference.PreferenceId] = preference
    ));

    Socket.joinRoom({
      session : this.session,
      deviceId : response.data.DeviceId,
      id_token : this.id_token
    });

    this.dispatch(updateCurrentDevice({
      DeviceId : response.data.DeviceId,
      DeviceName : response.data.DeviceName,
      Preferences : preferences
    }));
  }
}

function processUpdatePreference(response){
  if (response.status === status.Success){
    let preferences = {}
    response.data.Preferences.map(preference => (
      preferences[preference.PreferenceId] = preference
    ));
    this.dispatch(updateDevicePreferences({
      Preferences : preferences
    }));
  }
}

function processUpdateDevicePreferenceById(response){
  if (response.status === status.Success || response.status === status.UpdateFailed){

    Materialize.toast(response.message, 2000,('grey darken-4 font-weight-400 ' + (response.status !== status.UpdateFailed ? "green-text" : "red-text")));

    this.dispatch(updatePreferenceById({
      preference : response.data.preference
    }));
  }
}

function processSendMessage(response){
  if (response.status === status.Success){
    Materialize.toast(response.message, 2000,'grey darken-4 green-text font-weight-400');

    let message = response.data.message

    this.postSentAction(message._id)

    let messagePreference = {}

    message.messagePreference.map(p => (
      messagePreference[p.DeviceId] = p
    ))

    message.messagePreference = messagePreference

    this.dispatch(addMessages({
      messages : {
        [message._id] : message
      },
      messageType : this.messageType,
      fetchType : deviceActions.ADD_TO_SNEAKBOX
    }))
    this.dispatch(addMessages({
      messages : {
        [message._id] : message
      },
      messageType : this.messageType,
      fetchType : deviceActions.ADD_TO_SENT
    }))
  }
}

function processfetchMessages(response){
  if (response.status === status.Success){

    let messages = {}
    response.data.messages.map(message => {
      let messagePreference = {}

      message.messagePreference.map(p => (
        //we used deviceId to uniquely identify a preference, as it will be unique per message
        //also will be easy to update and fetch in the message
        messagePreference[p.DeviceId] = p
      ))

      message.messagePreference = messagePreference

      messages[message._id] = message
    });

    this.dispatch(addMessages({
      fetchType : this.fetchType,
      messages,
      messageType : this.messageType
    }))
  }
}

function processMessageReceived(response){
  if (response.status === status.Success){
    this.dispatch(updateMessagePreference(response.data.messageData))
  }
}