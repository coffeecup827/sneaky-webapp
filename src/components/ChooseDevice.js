import React, { Component } from 'react';
import { connect } from 'react-redux';
import {Redirect} from 'react-router-dom';
import Devices from './elements/devices'
import PropTypes  from 'prop-types';

class ChooseDevice extends Component {

  render() {

    if (!this.props.isLoggedIn) {
      return (
        <Redirect to='/' />
      )
    }

    return (
      <div className="container row" style={{marginTop : '150px'}}>
        <div className="col s12 m6 push-m3">
            {
              this.props.currentDeviceId ?
              <Redirect to='/dashboard' />
              :
              <Devices />
            }
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ userReducer,deviceReducer }) => {
  return {
    isLoggedIn : userReducer.isLoggedIn,
    currentDeviceId : deviceReducer.currentDevice.DeviceId
  };
}

const mapDispatchToProps = (dispatch) => {
  return {

  };
}

ChooseDevice.propTypes = {
  isLoggedIn : PropTypes.bool.isRequired,
  currentDeviceId : PropTypes.string
}

export default connect(mapStateToProps, mapDispatchToProps)(ChooseDevice);
