import React, { Component } from 'react';
import { Redirect,withRouter,Link } from 'react-router-dom';
import Layout from './elements/layout';
import General from './elements/profile/general';
import Preferences from './elements/profile/preferences';
import Devices from './elements/profile/devices';
import { connect } from 'react-redux';
import PropTypes  from 'prop-types';

class Profile extends Component {

  constructor(props){
    super(props);
    this.state = {
      layoutParams : {
          fab : {
            icon : "menu",
            color : "red"
          },
          Options : [
            {
              pathname : '/profile',
              fabColor : "blue",
              title : "General",
              icon : "perm_identity",
              isShowOnSideNav : true,
              id : "General"
            },
            {
              pathname : '/dashboard',
              fabColor : "grey darken-4",
              title : "Dashboard",
              icon : "dashboard",
              isShowOnSideNav : true,
              id : "Dashboard"
            },
            {
              pathname : '/profile/devices',
              fabColor : "green",
              title : "Devices",
              icon : "devices",
              isShowOnSideNav : true,
              id : "Devices"
            },
            {
              pathname : '/profile/preferences',
              fabColor : "yellow darken-1",
              title : "Preferences",
              icon : "settings",
              isShowOnSideNav : true,
              id : "Preferences"
            }
          ],
          optionStyle : "grey darken-4 style-for-profile-option hover hoverable",
          optionColor : "whitesmoke-text"
      }
    }
  }

  render() {

    if (!this.props.isLoggedIn) {
      return (
        <Redirect to='/' />
      )
    }

    return (
      <div className="row no-margin">

        <Layout params={this.state.layoutParams} >
          <General id="General" />
          <Preferences id="Preferences" />
          <Devices id="Devices" />
        </Layout>

      </div>
    );
  }
}


const mapStateToProps = ({ userReducer }) => {
  return {
    isLoggedIn : userReducer.isLoggedIn
  };
}

const mapDispatchToProps = (dispatch) => {
  return {

  };
}

Profile.propTypes = {
  isLoggedIn : PropTypes.bool.isRequired
}

Profile.defaultProps = {
  isLoggedIn: false
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Profile));
