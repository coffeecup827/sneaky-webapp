import React, { Component } from 'react';
import { Route } from 'react-router-dom'
import About from './about';
import ChooseDevice from './ChooseDevice';
import Dashboard from './Dashboard';
import Profile from './profile';

class App extends Component {

  render() {
    return (
      <div>

        <Route exact path="/" render={() => (
          <About />
        )}></Route>

        <Route exact path="/devices" render={() => (
          <ChooseDevice />
        )}></Route>

        <Route path="/dashboard" render={() => (
          <Dashboard />
        )}></Route>

        <Route path="/profile" render={() => (
          <Profile />
        )}></Route>

      </div>
    );
  }
}

export default App;
