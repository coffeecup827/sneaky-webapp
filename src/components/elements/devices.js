/*global $*/
import React, { Component } from 'react';
import { connect } from 'react-redux';
import CreateDevice from './deviceList/createDevice';
import DeviceList from './deviceList/deviceList';
import PropTypes  from 'prop-types';

class Devices extends Component {

  constructor(props) {
    super(props);
    $(document).ready(function(){
      $('.device-collapsible').collapsible();
    });
    this.state = {
      active : "collapsible-header grey darken-4 teal-text active",
      inActive : "collapsible-header grey darken-4 teal-text"
    }
  }

  render() {
    return (
      <div >

        <ul className="device-collapsible z-depth-4 hoverable" data-collapsible="accordion">
          {
            this.props.deviceCount > 0 &&
            <li className="border-top-inherit">
              <div className={`${this.state.active} border-top-inherit`}><i className="material-icons">filter_drama</i>Select Device</div>
              <div className="collapsible-body"><DeviceList /></div>
            </li>
          }

          <li className="border-bottom-inherit">
            <div className={this.props.deviceCount > 0 ? this.state.inActive : this.state.active}><i className="material-icons">create</i>Create Device</div>
            <div className="collapsible-body border-bottom-inherit border-bottom-none"><CreateDevice /></div>
          </li>
        </ul>

      </div>
    );
  }
}

const mapStateToProps = ({ userReducer }) => {
  return {
    deviceCount : userReducer.deviceCount
  };
}

const mapDispatchToProps = (dispatch) => {
  return {

  };
}

Devices.propTypes = {
  deviceCount : PropTypes.number.isRequired
}

export default connect(mapStateToProps, mapDispatchToProps)(Devices);
