/* global gapi */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { signinUser } from '../../../actions/userActions';
import { withRouter } from 'react-router-dom'
import PropTypes  from 'prop-types';
import {Socket} from '../../../helpers/socket';

class GSignIn extends Component {

  onSignIn = (googleUser) => {
    Socket.connect({
      signInActionParam : {
        googleUser
      },
      signInAction : this.props.onSignIn,
      dispatch : this.props.dispatch
    });
  }
  componentDidUpdate(prev){
    if(this.props.isLoggedIn !== prev.isLoggedIn){
      if (this.props.isLoggedIn && this.props.location.pathname === "/profile") {
        this.props.history.push("profile");
        return;
      }
      this.props.history.push(this.props.redirectTo);
    }
  }
  componentDidMount() {
    window.addEventListener('load',() =>{
      gapi.signin2.render(this.props.name, {
        'scope': 'profile email openid',
        'width': 130,
        'height': 40,
        'onsuccess': this.onSignIn
      });
    });
  }

  render() {
    return (
      <div id={this.props.name} className="gsignin"/>
    );
  }
}


const mapStateToProps = ({ userReducer }) => {
  return {
    redirectTo : userReducer.redirectTo,
    isLoggedIn : userReducer.isLoggedIn
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    onSignIn : (googleUser) => dispatch(signinUser(googleUser)),
    dispatch : dispatch
  };
}

GSignIn.propTypes = {
  redirectTo : PropTypes.string.isRequired,
  isLoggedIn : PropTypes.bool.isRequired,
  onSignIn : PropTypes.func.isRequired
}

GSignIn.defaultProps = {
  isLoggedIn: false
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(GSignIn));
