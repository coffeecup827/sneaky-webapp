/*global Materialize,$ */
import React, { Component } from 'react';
import Message from './message';
import moment from 'moment-timezone'
import MessageInfoModal from './messageInfoModal'
import {MessageTypes} from '../../../helpers/messgeType'

class MessageBox extends Component {

    constructor(props){
        super(props);
        this.state = {
            prevScrollPos : 0,
            showActions : "",
            isFirstLoad : true,
            modalMessage : null,
            messageInfoModalId : "MessageInfoModal"
        }
    }
    
    componentDidMount(){
        $(document).ready(function() {
            $('textarea#user-message-input').characterCounter();
        });
        $('.message-box').scrollTop($('.message-box')[0].scrollHeight);

        const messagesArray = Object.values(this.props.messages);
        if (messagesArray.length > 0) {
            this.setState({
                isFirstLoad : false
            })
        }

        $('.modal').modal();
    }

    componentDidUpdate(prevProps){

        const messagesArray = Object.values(this.props.messages);
        
        if(Object.keys(prevProps.messages).length !== messagesArray.length){

            if (this.props.messageFetchType === MessageTypes.NewMessage) {
                if (this.props.isSender) {
                    $('.message-box').scrollTop($('#' + messagesArray[messagesArray.length - 1]._id)[0].offsetTop);
                    this.props.resetSender();
                }
                else{
                    //show option to pull down to new message
                }
            }
            else if(this.state.isFirstLoad){
                $('.message-box').scrollTop($('#' + messagesArray[messagesArray.length - 1]._id)[0].offsetTop);
                this.setState({
                    isFirstLoad : false
                })
            }
            else{
                $('.message-box').scrollTop($('#' + Object.values(prevProps.messages)[0]._id)[0].offsetTop - 120);
            }
        }
    }

    onScroll = (e) => {
        if (this.state.prevScrollPos < e.target.scrollTop) {
            this.setState({
                prevScrollPos : e.target.scrollTop,
                showActions : ""
            },this.updateFooterOptions())

        }
        else{
            this.setState({
                prevScrollPos : e.target.scrollTop,
                showActions : "hide-message-helper"
            },this.updateFooterOptions())
        }
        
        if (e.target.scrollTop === 0) {
            this.setState({
                prevScrollPos : e.target.scrollTop,
                showActions : ""
            },() => {
                this.updateFooterOptions();
                this.props.oldMessageTrigger();
            })
        }
    }

    updateFooterOptions = () => {
        if(this.props.children){
            this.props.updateFooterOptions({
                showActions : this.state.showActions
            })
        }
    }

    viewInfo = (messageId) => {
        this.setState({
            modalMessage : this.props.messages[messageId]
        }, () => {
            $('#' + this.state.messageInfoModalId).modal('open');
        })
    }

    render() {

    let prevMessageDate = null;
    let prevSender = null;

    return (
        <div className="height-inherit message-box-container">

            <nav className={`message-helper ${this.state.showActions}`}>
                <div className={`nav-wrapper message-helper-nav  grey darken-4`}>
                    <ul className="right">
                        <li><a className="whitesmoke-text"><i className="material-icons left">more_vert</i><span className="hide-on-med-and-down">Menu</span></a></li>
                    </ul>
                </div>
            </nav>

            <div className="col s12 m12 l12 message-box"  onScroll={e => this.onScroll(e)}>

                <br />
                <br />
                <br />

                {
                    this.props.messages &&
                    Object.values(this.props.messages).map((message) => {

                        let currentMessageDate = moment(message.createdAt).clone().tz(moment.tz.guess()).format('D MMM YYYY');
                        
                        if (currentMessageDate !== prevMessageDate) {
                            let messageComponent =  
                                (
                                    <div key={message._id}>
                                        <h6 className="col s4 m1 l1 push-s4 push-m5 push-l5 blacky center message-date-divider z-depth-2">{currentMessageDate}</h6>
                                        <Message viewInfo={(messageId) => this.viewInfo(messageId)} message={message}/>
                                    </div>
                                )

                            prevMessageDate = currentMessageDate;
                            prevSender = message.byDeviceId;
                            return messageComponent;
                        }

                        if (prevSender !== message.byDeviceId) {
                            prevSender = message.byDeviceId;
                            return (
                                <div key={message._id}>
                                    <div className="message-divider"></div>
                                    <Message viewInfo={(messageId) => this.viewInfo(messageId)} message={message}/>
                                </div>
                            )
                        }
                        return (
                            <div key={message._id}>
                                <Message viewInfo={(messageId) => this.viewInfo(messageId)} message={message}/>
                            </div>
                        )

                    })
                }

                {
                    this.props.children && 
                    <div>   
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                    </div>
                }

            </div>
            
            {
                this.props.children && 
                this.props.children
            }
            
            <MessageInfoModal modalId={this.state.messageInfoModalId} message={this.state.modalMessage} />
            
        </div>
    );
  }
}

export default MessageBox;
