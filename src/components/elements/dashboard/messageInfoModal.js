import React, { Component } from 'react';
import { connect } from 'react-redux';
import moment from 'moment-timezone'

class MessageInfoModal extends Component {

  render() {

    let message = this.props.message;

    let isOwner = message ?  message.byDeviceId === this.props.CurrentDeviceId : false;

    let colorScheme = isOwner ?  "teal-text text-lighten-1" : "lime-text text-darken-3";

    let receivedBy = message ? Object.values(message.messagePreference).filter(x => x.IsReceived) : [];

    let sentTo = message ? Object.values(message.messagePreference).filter(x => !x.IsReceived) : [];

    return (
        <div id={this.props.modalId} className="modal bottom-sheet">
            {
                message &&
                <div className="modal-content grey darken-4 whitesmoke-text">
                    <div className="row no-margin">
                        <div className="col s10 m6 l6 push-l5 push-m5 push-s1">
                            <h6><span className={colorScheme}>Sender  :</span> {this.props.Devices[message.byDeviceId].DeviceName}</h6>
                            <h6><span className={colorScheme}>Message :</span> {message.message}</h6>
                            <h6><span className={colorScheme}>Sent At :</span> {moment(message.createdAt).clone().tz(moment.tz.guess()).format('h:mm a DD MMM YYYY')}</h6>

                            {
                                receivedBy.length >= 1 &&
                                <ul>
                                    <li><h6 className={colorScheme}>Received By :</h6></li>
                                    {
                                        receivedBy.map(x => (
                                            <li key={x._id} className="preference-list">
                                                <h6>{this.props.Devices[x.DeviceId].DeviceName}</h6>
                                                <h6 className="preference-list"><span className={colorScheme}>Received At :</span> {moment(x.ReceivedAt).clone().tz(moment.tz.guess()).format('h:mm a DD MMM YYYY')}</h6>
                                            </li>
                                        ))
                                    }
                                </ul>
                            }

                            {
                                sentTo.length >= 1 &&
                                <ul>
                                    <li><h6 className={colorScheme}>Sent To :</h6></li>
                                    {
                                        sentTo.map(x => (
                                            <li key={x._id} className="preference-list">
                                                <h6>{this.props.Devices[x.DeviceId].DeviceName}</h6>
                                            </li>
                                        ))
                                    }
                                </ul>
                            }
                        </div>
                    </div>
                </div>
            }
        </div>  
    );
  }
}


const mapStateToProps = ({ userReducer,deviceReducer }) => {
    return {
        CurrentDeviceId : deviceReducer.currentDevice.DeviceId,
        Devices : userReducer.devices
    };
  }

const mapDispatchToProps = (dispatch) => {
  return {

  };
}

export default connect(mapStateToProps, mapDispatchToProps)(MessageInfoModal);