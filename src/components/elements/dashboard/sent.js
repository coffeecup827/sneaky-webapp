import React, { Component } from 'react';
import MessageBox from './messageBox';
import { connect } from 'react-redux';
import {fetchForSent} from '../../../actions/deviceActions';
import {MessageTypes} from '../../../helpers/messgeType';

class Sent extends Component {

  componentDidMount(){
    if(!this.props.messages || Object.values(this.props.messages).length < 5){
      this.props.FetchMessages({
        id_token : this.props.id_token,
        token_exp : this.props.token_exp,
        googleUser : this.props.googleUser,
        deviceId : this.props.CurrentDeviceId,
        session : this.props.session,
        skipTo : this.props.messagePaginator,
        messageType : MessageTypes.OldMessage
      });
    }
  }

  fetchOldMessages = () => {
    this.props.FetchMessages({
      id_token : this.props.id_token,
      token_exp : this.props.token_exp,
      googleUser : this.props.googleUser,
      deviceId : this.props.CurrentDeviceId,
      session : this.props.session,
      skipTo : this.props.messagePaginator,
      messageType : MessageTypes.OldMessage
    });
  }

  render() {
    return (
      <div className="row no-margin grey darken-4 dashboard z-depth-2">

        <MessageBox 
            messageFetchType={this.props.messageFetchType}
            oldMessageTrigger={this.fetchOldMessages}
            messages={
            this.props.messages ?
            this.props.messages
            :
            {}
          }
        />
          
      </div>
    );
  }
}



const mapStateToProps = ({ userReducer,deviceReducer }) => {
  return {
      googleUser : userReducer.googleUser,
      id_token : userReducer.id_token,
      token_exp : userReducer.token_exp,
      CurrentDeviceId : deviceReducer.currentDevice.DeviceId,
      session : userReducer.session,
      messages : deviceReducer.Sent,
      messagePaginator : deviceReducer.SentPaginator,
      messageFetchType : deviceReducer.SentFetchType
  };
}


const mapDispatchToProps = (dispatch) => {
  return {
    FetchMessages : (data) => dispatch(fetchForSent(data))
  };
}


export default connect(mapStateToProps, mapDispatchToProps)(Sent);
