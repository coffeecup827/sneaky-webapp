/*global $*/
import React, { Component } from 'react';
import { connect } from 'react-redux';
import moment from 'moment-timezone'
import {messageReceived} from '../../../actions/deviceActions'

class Message extends Component {

    componentDidMount(){
        $('.message-option-dropdown-trigger-'+this.props.message._id).dropdown({
            constrainWidth: false,
        });
        if (this.props.message.byDeviceId !== this.props.CurrentDeviceId && !this.props.message.messagePreference[this.props.CurrentDeviceId].IsReceived) {
            this.props.MessageReceived({
                id_token : this.props.id_token,
                token_exp : this.props.token_exp,
                googleUser : this.props.googleUser,
                deviceId : this.props.CurrentDeviceId,
                session : this.props.session,
                messageId : this.props.message._id
            })
        }
    }

    viewInfo = () => {
        this.props.viewInfo(this.props.message._id);
    }

    deleteMessage = (e) => {
        console.log(e.target);
    }

    render() {

        let message = this.props.message;

        let time = moment(message.createdAt).clone().tz(moment.tz.guess()).format('h:mm a');

        let isOwner = message.byDeviceId === this.props.CurrentDeviceId;

        let colorScheme = isOwner ?  "teal-text text-lighten-1" : "lime-text text-darken-3";

        let messageDevice = this.props.Devices[message.byDeviceId];

        let messageStatusColorScheme = message.isReadByAll ? colorScheme : "grey-text"

        return (
        <div id={message._id} className="row message-row">
            <ul id={`message-option-dropdown-trigger-${message._id}`} className='dropdown-content z-depth-2 message-option-dropdown grey darken-4'>
                <li className="hover-blacky"><a className={`${colorScheme} message-option-dropdown-element`} onClick={this.viewInfo}>Info</a></li>
                {
                    isOwner &&
                    <li className="hover-blacky"><a className="red-text message-option-dropdown-element" onClick={(e) => this.deleteMessage(e)}>Delete</a></li>
                }
            </ul>
            
            <div className={`col s11 m7 l7 z-depth-2 message message-radius no-padding hoverable ${isOwner ?  "offset-s1 offset-m5 offset-l5 message-right" : "message-left"}`}>
                <div className={`row no-margin message-detail ${colorScheme}`}>
                    <div className="col s12 m12 l12 blacky message-header no-padding">

                        <div>
                            <h6 className="message-device-icon ">
                                <i className={"material-icons icon small-icon " + (messageDevice.IsLoggedIn ? "teal-text" : "orange-text")}
                                    title={(messageDevice.IsLoggedIn ? "Online" : "Offline")}
                                >
                                    {messageDevice.Type === "web" ? "computer" : "stay_current_portrait"}
                                </i>
                            </h6>
                            
                            <h6 className="message-device">
                                <span className="message-device-name">{messageDevice.DeviceName}</span>
                            </h6>
                        </div>
                        
                        <h6 className={`message-option message-option-dropdown-trigger-${message._id} hover`} 
                        data-activates={`message-option-dropdown-trigger-${message._id}`}>
                            <i className="material-icons sidebar-icon">more_vert</i>
                        </h6>
                        <h6 className="message-time">{time}</h6>
                    </div>
                </div>
                
                <div className="row message-body no-margin">
                    <div className="col s12 m12 message-content l12 blacky no-padding">
                        <p className="message-text left">
                            {message.message}
                        </p>
                        <p><i className={`material-icons icon message-status right ${messageStatusColorScheme}`}>done_all</i></p>
                    </div>
                </div>
            </div>
            
        </div>
        );
    }
}


const mapStateToProps = ({ userReducer,deviceReducer }) => {
    return {
        googleUser : userReducer.googleUser,
        id_token : userReducer.id_token,
        token_exp : userReducer.token_exp,
        CurrentDeviceId : deviceReducer.currentDevice.DeviceId,
        Devices : userReducer.devices,
        session : userReducer.session
    };
  }

const mapDispatchToProps = (dispatch) => {
  return {
    MessageReceived : (data) => dispatch(messageReceived(data))
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Message);
