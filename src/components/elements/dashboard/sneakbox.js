/*global Materialize,$ */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import MessageBox from './messageBox';
import {sendMessage,fetchForSneakBox} from '../../../actions/deviceActions';
import {MessageTypes} from '../../../helpers/messgeType'

class SneakBox extends Component {

  constructor(props){
    super(props);
    this.state = {
      message : "",
      FooterOptions : {
        showFooter : ""
      },
      isSender : false,
      disableUserInput : false
    }
  }
  
  componentDidMount(){
    if(!this.props.messages){
      this.props.FetchMessages({
        id_token : this.props.id_token,
        token_exp : this.props.token_exp,
        googleUser : this.props.googleUser,
        deviceId : this.props.DeviceId,
        session : this.props.session,
        skipTo : this.props.messagePaginator,
        messageType : MessageTypes.OldMessage
      });
    }
  }

  fetchOldMessages = () => {
    this.props.FetchMessages({
      id_token : this.props.id_token,
      token_exp : this.props.token_exp,
      googleUser : this.props.googleUser,
      deviceId : this.props.DeviceId,
      session : this.props.session,
      skipTo : this.props.messagePaginator,
      messageType : MessageTypes.OldMessage
    });
  }

  sendText = (e) => {
    e.preventDefault();

    if(this.state.message.trim() === "") return;

    this.setState({
      disableUserInput : true
    })

    this.props.SendMessage({
      id_token : this.props.id_token,
      token_exp : this.props.token_exp,
      googleUser : this.props.googleUser,
      deviceId : this.props.DeviceId,
      session : this.props.session,
      message : this.state.message,
      messageType : MessageTypes.NewMessage,
      postSentAction : () => {
          this.setState({
            message : "",
            isSender : true,
            disableUserInput : false
          })
        },
      requestFailedAction : () => {
        this.setState({
          disableUserInput : false
        })
      }
    });
  }

  resetSender = () => {
    this.setState({
      isSender : false
    })
  }

  updateMessge = (e) => {
    this.setState({
      message : e.target.value
    })
  }

  updateFooterOptions = (options) => {
    this.setState({
      FooterOptions : {
        showFooter : options.showActions
      }
    })
  }

  render() {
    return (
      <div className="row no-margin grey darken-4 dashboard z-depth-2">

        <MessageBox 
          isSender={this.state.isSender}
          resetSender={this.resetSender}
          messageFetchType={this.props.messageFetchType}
          updateFooterOptions={this.updateFooterOptions} 
          oldMessageTrigger={this.fetchOldMessages} 
          messages={this.props.messages || {}}
        >
          <form className={`col s12 m12 l12 textbox-form grey darken-4 z-depth-2  ${this.state.FooterOptions.showFooter}`} onSubmit={(e) => this.sendText(e)}>
            <div className="row">
              <div className="input-field col s12 m10 l10">
                <textarea 
                  disabled={this.state.disableUserInput}
                  onChange={(e) => this.updateMessge(e)} 
                  id="user-message-input" 
                  value={this.state.message} 
                  className="whitesmoke-text materialize-textarea textbox" 
                  data-length="1000"
                ></textarea>
              </div>
              <div className="col s12 m2 l2 center">
                <input 
                  type="submit" 
                  disabled={this.state.disableUserInput}
                  className="waves-effect waves-light btn textbox-submit" value="Send" 
                /> 
              </div>
            </div>
          </form>
        </MessageBox>

      </div>
    );
  }
}


const mapStateToProps = ({ userReducer,deviceReducer }) => {
  return {
      googleUser : userReducer.googleUser,
      id_token : userReducer.id_token,
      token_exp : userReducer.token_exp,
      DeviceId : deviceReducer.currentDevice.DeviceId,
      session : userReducer.session,
      messages : deviceReducer.SneakBox,
      messagePaginator : deviceReducer.SneakBoxPaginator,
      messageFetchType : deviceReducer.SneakBoxFetchType
  };
}


const mapDispatchToProps = (dispatch) => {
  return {
    SendMessage : (data) => dispatch(sendMessage(data)),
    FetchMessages : (data) => dispatch(fetchForSneakBox(data))
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(SneakBox);
