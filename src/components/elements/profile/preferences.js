import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchDevicePreference,updateDevicePreferenceById } from '../../../actions/deviceActions';
import PropTypes  from 'prop-types';

class Preferences extends Component {

  state = {

  }

  componentDidMount(){
    this.fetchPreferences();
  }

  static getDerivedStateFromProps(nextProps){
    return {
      ...nextProps.Preferences
    };
  }

  fetchPreferences = () => {
    if (this.props.DeviceId !== null) {
      this.props.fetchPreferences({
        id_token : this.props.id_token,
        googleUser : this.props.googleUser,
        token_exp : this.props.token_exp,
        deviceId : this.props.DeviceId,
        session : this.props.session
      })
    }
  }

  updateDevicePreference = (target) => {
    this.setState((prevState) => {
      return {
      [target.id] : {
        ...prevState[target.id],
        IsIncluded : target.checked
      }
    }},
    this.props.updateDevicePreferenceById({
      id_token : this.props.id_token,
      googleUser : this.props.googleUser,
      token_exp : this.props.token_exp,
      deviceId : this.props.DeviceId,
      preferenceId : target.id,
      isIncluded : target.checked,
      session : this.props.session
    }))
  }

  render() {

    if (this.props.DeviceId === null || Object.keys(this.props.Preferences).length === 0) {
      return(
        <div className="row whitesmoke-text" style={{margin : "0px"}}>
          <div className="col s12 l4 m4 z-depth-2 push-l4 push-m5 grey darken-4 margin-top-20px">
            <p className="whitesmoke-text center">{
              this.props.DeviceId === null ?
                "Please Choose A Device To View Its Preferences"
                :
                "Please Add One More Device To View Preferences"
            }</p>
            <a onClick={this.fetchPreferences} className="btn-flat blue-text underline devices-refresh-button center">
              REFRESH
            </a>
          </div>
        </div>
      )
    }

    return (
      <div className="row whitesmoke-text" style={{margin : "0px"}}>
        <div className="col s12 l4 m4 z-depth-2 push-l4 push-m5 grey darken-4 margin-top-20px">
          <table>
            <thead>
              <tr>
                  <th>Type</th>
                  <th>Device Name</th>
                  <th>Send</th>
              </tr>
            </thead>
            <tbody>
              {
                Object.values(this.state).map((preference) => (
                  <tr key={preference.PreferenceId}>
                    <td>
                      <i className={"material-icons device-type icon " + (preference.IsLoggedIn ? "teal-text" : "orange-text")}
                      title={(preference.IsLoggedIn ? "Online" : "Offline")}
                      >
                        {preference.Type === "web" ? "computer" : "stay_current_portrait"}
                      </i>
                    </td>
                    <td>
                      <h6 className="icon-text">{preference.DeviceName}</h6>
                    </td>
                    <td>
                      <input type="checkbox" className="filled-in"
                        onChange={(e) => this.updateDevicePreference(e.target)}
                        id={preference.PreferenceId} checked={preference.IsIncluded}
                      />
                      <label htmlFor={preference.PreferenceId}>{preference.IsIncluded ? "Included" : "Excluded"}</label>
                    </td>
                  </tr>
                ))
              }
            </tbody>
          </table>
          <a onClick={this.fetchPreferences} className="btn-flat blue-text underline devices-refresh-button center">
            REFRESH
          </a>
        </div>
      </div>
    );
  }
}


const mapStateToProps = ({ userReducer,deviceReducer }) => {
  return {
      googleUser : userReducer.googleUser,
      id_token : userReducer.id_token,
      token_exp : userReducer.token_exp,
      Preferences : deviceReducer.currentDevice.Preferences,
      DeviceId : deviceReducer.currentDevice.DeviceId,
      session : userReducer.session
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchPreferences : (data) => dispatch(fetchDevicePreference(data)),
    updateDevicePreferenceById : (data) => dispatch(updateDevicePreferenceById(data))
  };
}

Preferences.propTypes = {
  googleUser : PropTypes.object.isRequired,
  id_token : PropTypes.string.isRequired,
  session : PropTypes.string,
  token_exp : PropTypes.number.isRequired,
  Preferences : PropTypes.object.isRequired,
  DeviceId : PropTypes.string.isRequired,
  fetchPreferences : PropTypes.func.isRequired,
  updateDevicePreferenceById : PropTypes.func.isRequired
}

export default connect(mapStateToProps, mapDispatchToProps)(Preferences);
