/*global $*/
import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes  from 'prop-types';

class General extends Component {

  componentDidMount(){
    $(document).ready(function(){
      $('.modal').modal();
    });
  }

  deleteAccount = () => {
    console.log("Account Deleted");
  }

  render() {
    return (
      <div className="row">
        <div className="col s10 m4 push-m4 push-s1 margin-top-20px">
          <img alt="Profile" src={this.props.user.picture} className="profile-pic circle image-center z-depth-2 grey-darken-3" />
          <h6 className="whitesmoke-text center margin-top-20px">{this.props.user.name}</h6>
          <h6 className="whitesmoke-text center margin-top-20px">{this.props.user.email}</h6>
          <h6 className="center margin-top-20px"
            onClick={() => {$('#AccountDeleteConfirmationModal').modal('open')}}
          >
            <a className="btn-flat red-text underline">
              Delete Account
            </a>
          </h6>
        </div>

        <div id="AccountDeleteConfirmationModal" className="modal">
          <div className="modal-content grey darken-3">
            <h5 className="whitesmoke-text">Delete Account</h5>
            <h6 className="blue-text">Do you really want to delete your account?</h6>
            <h6 className="orange-text">All your data will be erased. This cannot be undone.</h6>
          </div>
          <div className="modal-footer grey darken-3">
            <a onClick={this.deleteAccount} className="modal-action modal-close btn-flat red-text">Delete</a>
          </div>
        </div>
      </div>
    );
  }
}


const mapStateToProps = ({ userReducer }) => {
  return {
    user : userReducer.user
  };
}

const mapDispatchToProps = (dispatch) => {
  return {

  };
}


General.propTypes = {
  user : PropTypes.shape({
    picture: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    email: PropTypes.string.isRequired
  })
}

export default connect(mapStateToProps, mapDispatchToProps)(General);
