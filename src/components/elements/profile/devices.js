import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchDevices } from '../../../actions/userActions';
import PropTypes  from 'prop-types';

class Devices extends Component {

  componentDidMount(){
    if(!this.props.devices){
      this.fetchDevices();
    }
  }

  fetchDevices = () => {
    this.props.fetchDevices({
        id_token : this.props.id_token,
        googleUser : this.props.googleUser,
        token_exp : this.props.token_exp,
        session : this.props.session
    });
  }

  deleteDevice = (device) => {
    console.log("Device Deleted");
  }

  render() {

    if (this.props.devices.length === 0) {
      return(
        <div className="row whitesmoke-text" style={{margin : "0px"}}>
          <div className="col s12 l4 m4 z-depth-2 push-l4 push-m5 grey darken-4 margin-top-20px">
            <p className="whitesmoke-text center">
              Please Add A Device
            </p>
            <a onClick={this.fetchPreferences} className="btn-flat blue-text underline devices-refresh-button center">
              REFRESH
            </a>
          </div>
        </div>
      )
    }

    return (
      <div className="row whitesmoke-text" style={{margin : "0px"}}>
        <div className="col s12 l4 m4 z-depth-2 push-l4 push-m5 grey darken-4 margin-top-20px">
          <table>
            <thead>
              <tr>
                  <th>Type</th>
                  <th>Device Name</th>
                  <th>Delete</th>
              </tr>
            </thead>
            <tbody>
              {
                Object.values(this.props.devices).map((device) => (
                  <tr key={device._id}>
                    <td>
                      <i className={"material-icons device-type icon " + (device.IsLoggedIn ? "teal-text" : "orange-text")}
                      title={(device.IsLoggedIn ? "Online" : "Offline")}
                      >
                        {device.Type === "web" ? "computer" : "stay_current_portrait"}
                      </i>
                    </td>
                    <td>
                      <h6 className="icon-text">{device.DeviceName}</h6>
                    </td>
                    <td>
                      <i className="material-icons icon red-text hover"
                        title={"Delete " + device.DeviceName}
                        onClick={(e) => this.deleteDevice({
                          DeviceId : device._id,
                          DeviceName : device.DeviceName
                        })}
                      >delete</i>
                    </td>
                  </tr>
                ))
              }
            </tbody>
          </table>
          <a onClick={this.fetchDevices} className="btn-flat blue-text underline devices-refresh-button center">
            REFRESH
          </a>
        </div>
      </div>
    );
  }
}


const mapStateToProps = ({ userReducer }) => {
  return {
    googleUser : userReducer.googleUser,
    devices : userReducer.devices,
    id_token : userReducer.id_token,
    token_exp : userReducer.token_exp,
    session : userReducer.session
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchDevices : (data) => dispatch(fetchDevices(data))
  };
}

Devices.propTypes = {
  googleUser : PropTypes.object.isRequired,
  id_token : PropTypes.string.isRequired,
  session : PropTypes.string,
  token_exp : PropTypes.number.isRequired,
  devices : PropTypes.object.isRequired,
  fetchDevices : PropTypes.func.isRequired
}

export default connect(mapStateToProps, mapDispatchToProps)(Devices);
