import React, { Component } from 'react';
import { connect } from 'react-redux';
import {selectDevice} from '../../../actions/deviceActions';
import PropTypes  from 'prop-types';

class DeviceList extends Component {

  selectDevice = (device) => {
    this.props.SelectDevice({
        id_token : this.props.id_token,
        device : {
          DeviceId : device.DeviceId,
          DeviceName : device.DeviceName
        },
        googleUser : this.props.googleUser,
        token_exp : this.props.token_exp,
        session : this.props.session
    });
  }

  render() {
    return (
      <div className="row white-text" style={{margin : "0px"}}>
        <div className="col s12 l6 m4 z-depth-2 push-l3 push-m4 grey darken-4">
          {
            Object.values(this.props.devices).map((x) =>
              <div className="row" style={{margin : "0px",marginTop : "5px"}} key={x._id} >
                <h6 className="col s10 l10 m10 icon-text whitesmoke-text">{x.DeviceName}</h6>
                <h6 className="col s2 m2 l2">
                  <i className="material-icons teal-text icon hover"
                    title={"Login as " + x.DeviceName}
                    onClick={(e) => this.selectDevice({
                      DeviceId : x._id,
                      DeviceName : x.DeviceName
                    })}
                  >send</i>
                </h6>
              </div>
            )
          }
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ userReducer }) => {
  return {
    googleUser : userReducer.googleUser,
    devices : userReducer.devices,
    id_token : userReducer.id_token,
    token_exp : userReducer.token_exp,
    session : userReducer.session
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    SelectDevice : (data) => dispatch(selectDevice(data))
  };
}

DeviceList.propTypes = {
  googleUser : PropTypes.object.isRequired,
  id_token : PropTypes.string.isRequired,
  session : PropTypes.string,
  token_exp : PropTypes.number.isRequired,
  SelectDevice : PropTypes.func.isRequired,
  devices : PropTypes.object.isRequired
}


export default connect(mapStateToProps, mapDispatchToProps)(DeviceList);
