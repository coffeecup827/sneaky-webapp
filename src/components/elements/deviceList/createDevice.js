/*global Materialize*/
import React, { Component } from 'react';
import { connect } from 'react-redux';
import {createDevice} from '../../../actions/deviceActions';
import PropTypes  from 'prop-types';

class CreateDevice extends Component {

  constructor(props) {
    super(props);
    this.state = {
      deviceName : ""
    }
  }

  updateDeviceName = (e) => {
    this.setState({
      deviceName : e.target.value
    })
  }

  createDevice = () => {
    if (this.state.deviceName.length >= 5 && this.state.deviceName.length <= 12) {
      this.props.CreateDevice({
        id_token : this.props.id_token,
        deviceName : this.state.deviceName,
        googleUser : this.props.googleUser,
        token_exp : this.props.token_exp,
        session : this.props.session
      });
    }
    else{
      Materialize.toast('Device Name must be between 5-12 characters', 2000,'grey darken-4')
    }
  }

  render() {
    return (
      <div>
        <div className="row center teal-text no-margin">
          <div className="input-field col l6 m4 s12 push-m4 push-l2 margin-top-20px">
            <i className="material-icons prefix">desktop_windows</i>
            <input id="device_name" type="text" className="validate" value={this.state.deviceName} onChange={(e) => this.updateDeviceName(e)} />
            <label htmlFor="device_name">Device Name</label>
          </div>
          <div className="col m1 s12 push-m2">
            <a className="waves-effect waves-light btn margin-top-20px font-rem" onClick={(e) => this.createDevice(e)}>Create</a>
          </div>
        </div>
        <div className="row no-margin">
          <p className="margin-top-20px center pink-text">Device Name must be between 5-12 characters</p>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ userReducer }) => {
  return {
    googleUser : userReducer.googleUser,
    id_token : userReducer.id_token,
    token_exp : userReducer.token_exp,
    session : userReducer.session
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    CreateDevice : (data) => dispatch(createDevice(data))
  };
}

CreateDevice.propTypes = {
  googleUser : PropTypes.object,
  session : PropTypes.string,
  id_token : PropTypes.string.isRequired,
  token_exp : PropTypes.number.isRequired,
  CreateDevice : PropTypes.func.isRequired
}

export default connect(mapStateToProps, mapDispatchToProps)(CreateDevice);
