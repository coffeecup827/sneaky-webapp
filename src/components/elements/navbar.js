/*global $*/

import React, { Component } from 'react';
import { connect } from 'react-redux';
import GSignIn from './navbar/gsignin';
import { signOutUser } from '../../actions/userActions';
import { Link } from 'react-router-dom';
import PropTypes  from 'prop-types';

class NavBar extends Component {
  componentDidMount() {
    $( document ).ready(function(){
      $(".user-option-dropdown-button").dropdown();
      $('.modal').modal();
    })
  }

  componentDidUpdate(){
    if(this.props.isLoggedIn){
      $('#LogInModal').modal('close');
      $(".user-option-dropdown-button").dropdown();
    }
  }

  getRedirectUrl = () => {
    if (this.props.isLoggedIn) {
      return this.props.currentDeviceId ? "/dashboard" : "/devices";
    }
    else {
      return "/";
    }
  }

  logoutUser = () => {
    this.props.signOutUser({
      id_token : this.props.id_token,
      googleUser : this.props.googleUser,
      token_exp : this.props.token_exp,
      deviceId : this.props.currentDeviceId,
      session : this.props.session
    });
  }

  closeSideNav = () => {
    $('.button-collapse').sideNav('hide');
  }

  render() {
    return (
      <div>

        <ul id='user-option-dropdown' className='dropdown-content whitesmoke-text grey darken-4'>
          <li className="hover-blacky">
            <Link to={{pathname: "/profile"}} className="teal-text font-weight-400">Profile</Link>
          </li>
          <li className="divider blacky"></li>
          <li className="hover-blacky"><a onClick={this.logoutUser} className="red-text font-weight-400">Logout</a></li>
        </ul>

        <nav>
          <div className="nav-wrapper grey darken-4 z-depth-2">

            <Link
              to={{
                pathname: this.getRedirectUrl()
              }}
              className="brand-logo margin-left-15px"
              style={{color: "#9e9e9e",fontSize : "180%"}}
            >
              Sneaky
            </Link>

            <a data-activates="mobile-demo" className="button-collapse"><i className="material-icons whitesmoke-text">menu</i></a>

            <ul className="right hide-on-med-and-down">

              <li style={{display : this.props.isLoggedIn ? "inline-block" : "none"}}>
                <a
                  style={{fontSize : "16px"}}
                  className='user-option-dropdown-button width-150px whitesmoke-text'
                  data-beloworigin="true"
                  data-activates='user-option-dropdown'
                >
                  {this.props.user.given_name}
                </a>
              </li>

              <li style={{display : !this.props.isLoggedIn ? "inline-block" : "none"}}>
                <a style={{fontSize : "16px"}} onClick={() => {$('#LogInModal').modal('open')}}>
                  Login / Signup
                </a>
              </li>
            </ul>

            <ul className="side-nav" id="mobile-demo">

              <li>
                <Link to={{
                  pathname: this.getRedirectUrl()
                }}
                onClick={this.closeSideNav}
                >
                  Sneaky
                </Link>
              </li>

              <div style={{display : this.props.isLoggedIn ? "block" : "none"}}>
                <li><Link to={{pathname: "/profile"}} onClick={this.closeSideNav}>Profile</Link></li>
                <li><a onClick={this.logoutUser} className="red-text">Logout</a></li>
              </div>

              <li style={{display : !this.props.isLoggedIn ? "block" : "none"}}>
                <a className="bottom" onClick={() => {
                  $('#LogInModal').modal('open');
                  $('.button-collapse').sideNav('hide');
                }}>
                  Login / Signup
                </a>
              </li>
            </ul>

          </div>
        </nav>


        <div id="LogInModal" className="modal bottom-sheet">
          <div className="modal-content grey darken-4">
            <div className="row center no-margin">
              <div className="col s10 m4 push-m4 push-s1 margin-top-20px">
                <img className="login-pic round-image z-depth-2 grey-darken-3" />
                <GSignIn name="login" />
              </div>
            </div>
          </div>
        </div>

      </div>
    );
  }
}


const mapStateToProps = ({ userReducer,deviceReducer }) => {
  return {
    googleUser : userReducer.googleUser,
    user : userReducer.user,
    isLoggedIn : userReducer.isLoggedIn,
    id_token : userReducer.id_token,
    token_exp : userReducer.token_exp,
    currentDeviceId : deviceReducer.currentDevice.DeviceId,
    session : userReducer.session
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    signOutUser : (data) => dispatch(signOutUser(data))
  };
}

NavBar.propTypes = {
  googleUser : PropTypes.object,
  user : PropTypes.shape({
    given_name : PropTypes.string
  }),
  isLoggedIn : PropTypes.bool.isRequired,
  id_token : PropTypes.string,
  session : PropTypes.string,
  token_exp : PropTypes.number,
  currentDeviceId : PropTypes.string,
  signOutUser : PropTypes.func.isRequired
}

export default connect(mapStateToProps, mapDispatchToProps)(NavBar);
