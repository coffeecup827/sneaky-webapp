/*global $*/
import React, { Component } from 'react';
import { Route,withRouter,Link } from 'react-router-dom';
import PropTypes  from 'prop-types';

class Layout extends Component {

  constructor(props){
    super(props);
    this.state = {
      height : "",
      showFabText : false
    }
  }

  updateHeight = () => {
    this.setState({
      height : window.innerHeight - 64
    });
  }

  toggleFabText = () => {
    this.setState({
      showFabText : !this.state.showFabText
    });
  }

  componentDidMount(){
    this.updateHeight();
    window.addEventListener("resize", this.updateHeight);
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.updateHeight);
  }

  render() {

    let fab = this.props.params.fab;
    let options = this.props.params.Options;
    let optionStyle = this.props.params.optionStyle;
    let optionColor = this.props.params.optionColor;

    return (
      <div style={{height : this.state.height}}>
        {/*Fab Generator */}
        <div className="fixed-action-btn click-to-toggle hide-on-large-only">
          <a onClick={() => this.toggleFabText()} className={"btn-floating btn-large " + fab.color}>
            <i className="large material-icons">{fab.icon}</i>
          </a>
          <ul>
            {
              options.map(option => (
                <li key={option.title}>
                  <div className={`fab-text ${this.state.showFabText ? "show-fab-text" : "hide-fab-text"}`}>
                    <h6>{option.title}</h6>
                  </div>
                  <Link to={{
                    pathname: option.pathname
                  }}
                  onClick={() => {$('.fixed-action-btn').closeFAB();this.toggleFabText();}}
                   className={"btn-floating " + option.fabColor} title={option.title}
                  >
                    <i className="material-icons">{option.icon}</i>
                  </Link>
                </li>
              ))
            }
          </ul>
        </div>

        {/*Side Bar Generator*/}
        <div className="col l2 hide-on-med-and-down height-inherit grey darken-4">
          <div className="blacky z-depth-2 style-for-side-bar-menu">
            {options.filter(option => option.isShowOnSideNav).map(option => (
              <Link key={option.title} to={{
                pathname: option.pathname
              }}>
                <div className={optionStyle}>
                  <h6 className={optionColor}>
                    <i className="material-icons sidebar-icon">{option.icon}</i>
                    <span className="profile-option-heading">{option.title}</span>
                  </h6>
                </div>
              </Link>
            ))}
          </div>
        </div>

        {/*Route Generator*/}
        <div className="col l10 m10 push-m1 s12 height-inherit blacky static">
          {
            options.filter(option => option.isShowOnSideNav).map(option => {
              return (
              <Route exact path={option.pathname}
                key={option.title}
                render={() => (
                  <div className="blacky padding-2px height-inherit">
                    <h5 className={optionColor}>{option.title}</h5>
                    {this.props.children.filter(child => child.props.id === option.id)}
                  </div>
                )}
              >
              </Route>
            )})
          }
        </div>

      </div>
    );
  }

}

Layout.propTypes = {
  params : PropTypes.shape({
    fab: PropTypes.object.isRequired,
    Options: PropTypes.array.isRequired,
    optionStyle: PropTypes.string.isRequired,
    optionColor: PropTypes.string.isRequired
  }),
  children : PropTypes.arrayOf(PropTypes.element).isRequired
}

export default withRouter(Layout);
