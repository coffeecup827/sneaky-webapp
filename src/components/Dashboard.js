import React, { Component } from 'react';
import { Redirect,withRouter,Link } from 'react-router-dom';
import Layout from './elements/layout';
import Inbox from './elements/dashboard/inbox';
import Sent from './elements/dashboard/sent';
import SneakBox from './elements/dashboard/sneakbox';
import Profile from './profile';
import { connect } from 'react-redux';

class Dashboard extends Component {

  constructor(props){
    super(props);
    this.state = {
      layoutParams : {
          fab : {
            icon : "menu",
            color : "red"
          },
          Options : [
            {
              pathname : '/dashboard',
              fabColor : "blue",
              title : "Sneak Box",
              icon : "question_answer",
              isShowOnSideNav : true,
              id : "SneakBox"
            },
            {
              pathname : '/dashboard/inbox',
              fabColor : "yellow",
              title : "Inbox",
              icon : "message",
              isShowOnSideNav : true,
              id : "Inbox"
            },
            {
              pathname : '/dashboard/sent',
              fabColor : "grey darken-4",
              title : "Sent",
              icon : "comment",
              isShowOnSideNav : true,
              id : "Sent"
            },
            {
              pathname : '/profile',
              fabColor : "green",
              title : "Profile",
              icon : "perm_identity",
              isShowOnSideNav : true,
              id : "Profile"
            }
          ],
          optionStyle : "grey darken-4 style-for-profile-option hover hoverable",
          optionColor : "whitesmoke-text"
      }
    }
  }

  render() {

    if (!this.props.isLoggedIn) {
      return (
        <Redirect to='/' />
      )
    }

    return (
      <div className="row no-margin">

        <Layout params={this.state.layoutParams} >
          <SneakBox id="SneakBox" />  
          <Inbox id="Inbox" />
          <Sent id="Sent" />
          <Profile id="Profile" />
        </Layout>

      </div>
    );
  }
}

const mapStateToProps = ({ userReducer }) => {
  return {
    isLoggedIn : userReducer.isLoggedIn
  };
}

const mapDispatchToProps = (dispatch) => {
  return {

  };
}

export default  withRouter(connect(mapStateToProps, mapDispatchToProps)(Dashboard));
