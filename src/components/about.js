import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes  from 'prop-types';

class About extends Component {

  render() {

    if (this.props.isLoggedIn) {
      return (
        <div>
          {
            this.props.currentDeviceId ?
            <Redirect to='/dashboard' />
            :
            <Redirect to='/devices' />
          }
        </div>
      )
    }

    return (
      <div className="container row" style={{marginTop : '200px'}}>
        <div className="col s12 m12">

          <div className="card  grey darken-4 z-depth-3">
            <div className="card-content">

              <h3 className="center whitesmoke-text"><i className="material-icons teal-text" style={{marginRight : "30px",fontSize : "32px"}}>build</i>Work In Progress</h3>

            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ userReducer,deviceReducer }) => {
  return {
    isLoggedIn : userReducer.isLoggedIn,
    currentDeviceId : deviceReducer.currentDevice.DeviceId
  };
}

const mapDispatchToProps = (dispatch) => {
  return {

  };
}

About.propTypes = {
  isLoggedIn : PropTypes.bool.isRequired,
  currentDeviceId : PropTypes.string
}

export default connect(mapStateToProps, mapDispatchToProps)(About);
