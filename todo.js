const todo = {
  ModulesToUse :{
    Crypto
  },
  Server :{
    Authoriztion :  {
      ToDo : "
        Delete Auth on socket close.

        For id_token reload case, it'll fail the userSession and userDeviceVerification, check like if id_token $ne, then check
        for session and deviceId match, if yes, proceed. (none can gen id_token)

        (keep id_token check(id_token level encapsulation))

        May be Eureka!",
      Hint : "You have to verify the user and device on the helper methods to use globally! Wow my design!"
    },
    Encryption :{
      Keys : "Store the keys in config vars"
    },
    ResponseBody : {
      Decrypt : "Decrypt the data"
    }
    MongoDB : {
      Storing : "Encrypt messages of the user (sub not needed caz i always fetch from id_token which can be only verified by me)"
    }
  }
}
